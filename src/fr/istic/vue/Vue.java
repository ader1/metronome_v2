package fr.istic.vue;

import fr.istic.command.Cursseur;
import fr.istic.command.Dec;
import fr.istic.command.Inc;
import fr.istic.command.Start;
import fr.istic.command.Stop;

public interface Vue {
void start();
void stop();
void inc();
void dec();
void initialise ();
void setCursseur(Cursseur c );
void setStart(Start c );
void setDec(Dec c);
void setInc (Inc c);
float getvaleurslider();
void setlabel(int f );
void allumerled1();
void eteindled();
void setStope(Stop c);
 void updateMeasure(int measure);
 void allumerled2();
	

}
