package fr.istic.vue;

import fr.istic.controlleur.Controlleur;
import fr.istic.materiel.Materiel;

public class AdapterInverse implements Controlleur {
	
	private Vue vue ;

	@Override
	public void start() {
		// TODO Auto-generated method stub
	
		Materiel.getClavier().boutonPresser(1);
		
		
	}

	@Override
	public void inc() {
		// TODO Auto-generated method stub
		
		Materiel.getClavier().boutonPresser(3);
		
	}

	@Override
	public void dec() {
		// TODO Auto-generated method stub
		Materiel.getClavier().boutonPresser(4);	
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		Materiel.getClavier().boutonPresser(2);
		
	}

	@Override
	public void cursseur() {
		// TODO Auto-generated method stub
		
		Materiel.getMolette().mettreAjour(vue.getvaleurslider());
	}

	@Override
	public void updateTempo() {
		// TODO Auto-generated method stub
		
     
	}

	@Override
	public void tic() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMesure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setVue(Vue v) {
		// TODO Auto-generated method stub
		vue = v;
	}

}
