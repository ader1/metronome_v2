package fr.istic.vue;

import fr.istic.command.Command;
import fr.istic.command.Cursseur;
import fr.istic.command.Dec;
import fr.istic.command.Inc;
import fr.istic.command.Start;
import fr.istic.command.Stop;
import fr.istic.materiel.Materiel;
import fr.istic.materiel.MaterielImpl;
import javafx.application.Platform;

public class Adaptateur implements Vue{

	private Command start;
	private Command stop;
	private Command inc;
	private Command dec;
	private Command cursseur;
	
	private VueImpl vue;
	// l'aucienne valeur de slider
	int valeurAuc;
	
	
	@Override
	public void start() {
		//hardware
		start.execute();
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		stop.execute();
	}

	@Override
	public void inc() {
		// TODO Auto-generated method stub
		inc.execute();
	}

	@Override
	public void dec() {
		// TODO Auto-generated method stub
		dec.execute();
	}

	@Override
	public void initialise() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCursseur(Cursseur c) {
		// TODO Auto-generated method stub
		cursseur=c;
	}

	@Override
	public void setStart(Start c) {
		// TODO Auto-generated method stub
		start = c;
		
	}

	@Override
	public void setDec(Dec c) {
		// TODO Auto-generated method stub
		dec=c;
		
	}

	@Override
	public void setInc(Inc c) {
		// TODO Auto-generated method stub
		inc=c;
		
	}

	@Override
	public float getvaleurslider() {
		// TODO Auto-generated method stub
	
		return Materiel.getMolette().position();
	}

	@Override
	public void setlabel(int f) {
		// TODO Auto-generated method stub7
   
    Platform.runLater(() -> vue.setlabel(f));
   Materiel.getAfficheur().afficherTempo(f);
	}

	@Override
	public void allumerled1() {
		// TODO Auto-generated method stub
		 vue.allumerled1();
		 Materiel.getAfficheur().allumerLED(1);
		 Materiel.getEmetteurSonore().emettreClic();
		Materiel.getAfficheur().eteindreLED(1);		
	}

	@Override
	public void eteindled() {
		 vue.eteindled();
		 Materiel.getAfficheur().eteindreLED(1);
		 Materiel.getAfficheur().eteindreLED(2);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setStope(Stop c) {
		// TODO Auto-generated method stub
		stop = c;
		
	}

	@Override
	public void updateMeasure(int measure) {
		// TODO Auto-generated method stub
		vue.updateMeasure(measure);
		
	}

	@Override
	public void allumerled2() {
		// TODO Auto-generated method stub
		vue.allumerled2();
		
		Materiel.getAfficheur().allumerLED(2);
		Materiel.getEmetteurSonore().emettreClic();
		Materiel.getAfficheur().eteindreLED(2);
	}

	public void poll() {
		// TODO Auto-generated method stub
		
	if (Materiel.getClavier().touchePressee(1)) {
		
		start();
		Materiel.getClavier().toucheRelacher(1);
	}
	if(Materiel.getClavier().touchePressee(2)){
		
		stop();
		Materiel.getClavier().toucheRelacher(2);
		
	}
	if(Materiel.getClavier().touchePressee(3)){
		
		inc();
		Materiel.getClavier().toucheRelacher(3);
	}
    if(Materiel.getClavier().touchePressee(4)){
		
		dec();
		Materiel.getClavier().toucheRelacher(4);
	}
	//mollete
    int valeurCursseur = (int) Materiel.getMolette().position();
 
    if(valeurCursseur!=valeurAuc){
    	   cursseur.execute();

    }
    valeurAuc = valeurCursseur;
	}

	public void setVue (VueImpl v){
		vue=v;
	}

}
