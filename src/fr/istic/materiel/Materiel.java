package fr.istic.materiel;



public class  Materiel {
	static MaterielImpl materiel;
	
    public static void  setMatriel(MaterielImpl m ){
    	materiel =m;
    }
    public static Horloge getHorloge() {
        return  materiel;
    }

    public static Clavier getClavier() {
        return materiel;
    }

    public static Molette getMolette() {
        return materiel;
    }

    public static EmetteurSonore getEmetteurSonore() {
        return materiel;
    }

    public static Afficheur getAfficheur() {
        return materiel;
    }

}
