package fr.istic.materiel;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import fr.istic.command.Command;

import fr.istic.vue.VueImpl;

public class MaterielImpl implements Afficheur, Clavier, EmetteurSonore, Molette, Horloge   {

	private VueImpl vue;

	private boolean start = false;
	private boolean stop = false;
	private boolean inc = false;
	private boolean dec = false;
	
	private float slider = 0;
	
	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> beeperHandle;

	

	@Override
	public void emettreClic() {
		// TODO Auto-generated method stub
		System.out.println("emettreClic");
	}

	@Override
	public boolean touchePressee(int i) {
	
		if(i == 1 ) {return start;  } 
		else if(i==2) return stop;
		else if (i==3) return inc;
		else if (i==4 )  return dec ;
		else return false;
	}

	@Override
	public void allumerLED(int numLED) {
		// TODO Auto-generated method stub
		if (numLED == 1 )System.out.println("allumerLED 1");
		else System.out.println("allumerLED 2");
		

	}

	@Override
	public void eteindreLED(int numLED) {
		// TODO Auto-generated method stub
		if (numLED == 1 )System.out.println("eteindreLED 1");
		else System.out.println("eteindreLED 2");

	}

	@Override
	public void afficherTempo(int valeurTempo) {
		// TODO Auto-generated method stub
		System.out.println("afficherTempo");
	

	}

	@Override
	public float position() {
		// TODO Auto-generated method stub
		
		return slider ;
	}

	@Override
	public void mettreAjour(float valeurs) {
		// TODO Auto-generated method stub
		slider=valeurs;
		
	}

	@Override
	public void boutonPresser(int i) {
		// TODO Auto-generated method stub
		if(i == 1 )  start = true; 
		else if(i==2) stop = true ;
		else if (i==3)  inc = true ;
		else { dec=true ;}
		
	}

	@Override
	public void activerPeriodiquement(Command cmd, float periodeEnMiliSecondes) {
		// TODO Auto-generated method stub
		scheduler = Executors.newScheduledThreadPool(1);

		final Runnable beeper = new Runnable() {
			public void run() {
				cmd.execute();
			}
		};
		beeperHandle = scheduler.scheduleAtFixedRate(beeper, 0, (int) (periodeEnMiliSecondes * 1000), MILLISECONDS);
		
		
	}

	@Override
	public void desactiverHorloge() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toucheRelacher(int i) {
		// TODO Auto-generated method stub

      if (i == 1){start =false;}
      if(i == 2){stop = false;}
      if (i == 3){inc = false;}
      else dec =false;
		
	}

	

}
