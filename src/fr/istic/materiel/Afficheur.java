package fr.istic.materiel;

public interface Afficheur {
	public void allumerLED(int numLED);

	public void eteindreLED(int numLED);

	public void afficherTempo(int valeurTempo);

}
