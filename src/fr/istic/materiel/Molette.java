package fr.istic.materiel;

public interface Molette {
	
	float position();
	void mettreAjour(float valeurs);

}
