package fr.istic.command;

public interface Command {
/**
 * execute la commande
 */
	void execute();
}
